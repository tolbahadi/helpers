'use strict';
// jshint camelcase: false

var request = require('./request_wrapper').request,
    verror = require('verror'),
    _ = require('underscore');

function successHandler (result) {
    return JSON.parse(result.contents);
}

function wrapFail (error, extra) {
    var werror = new verror.WError(error, 'FacebookWrapper');
    _.extend(werror, extra);
    return werror;
}

exports.postPhotoToFacebook = function (photoLink, albumId, message, accessToken) {
    return request({
        uri: 'https://graph.facebook.com/' + albumId + '/photos',
        method: 'post',
        form: {
            access_token: accessToken,
            url: encodeURI(photoLink),
            message: message
        }
    }).then(successHandler).fail(function (error) {
        return wrapFail(error, {photoLink: photoLink, albumId: albumId, accessToken: accessToken, message: message});
    });
};

exports.postMessageToFacebook = function (pageId, message, accessToken) {
    return request({
        uri: 'https://graph.facebook.com/' + pageId + '/feed',
        method: 'post',
        form: {
            access_token: accessToken,
            message: message
        }
    }).then(successHandler).fail(function (error) {
        return wrapFail(error, {pageId: pageId, accessToken: accessToken, message: message});
    });
};