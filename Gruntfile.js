module.exports = function (grunt) {
    'use strict';

    grunt.loadNpmTasks('grunt-simple-mocha');
    grunt.loadNpmTasks('grunt-contrib-jshint');

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            options: grunt.file.readJSON('.jshintrc'),
            all: ['Gruntfile.js', '*.js'],
            test: {
                options: grunt.file.readJSON('test/.jshintrc'),
                files: {
                    src: ['test/**/*.js']
                }
            }
        },
        simplemocha: {
            options: {
                globals: ['should'],
                require: ['should'],
                // timeout: 3000,
                ignoreLeaks: false,
                // grep: '*-test',
                ui: 'bdd',
                // bail: true,
                reporter: 'spec'
            },
            all: {
                src: ['test/**/*.js']
            }
        }
    });


    // Default task(s).
    grunt.registerTask('test', 'simplemocha:all');
    grunt.registerTask('default', ['jshint', 'test']);

};