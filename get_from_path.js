'use strict';

var _ = require('underscore');

module.exports = function (source, parts) {
    if (!source) {
        return null;
    }
    if (typeof parts === 'string') {
        parts = parts.split('.');
    }
    for (var i = 0, l = parts.length; i < l; i++) {
        if (_.has(source, parts[i])) {
            source = source[parts[i]];
        } else {
            return null;
        }
    }
    return source;
};