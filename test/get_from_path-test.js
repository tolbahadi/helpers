'use strict';

var should = require('should'),
    getFromPath = require('../get_from_path');

should = null;

describe('getFromPath module', function () {
    describe('getFromPath#getFromPath', function () {
        it('String path', function () {
            var value = getFromPath({
                one: {
                    two: 2
                }
            }, 'one.two');
            value.should.be.equal(2);
        });
        it('Array path', function () {
            var value = getFromPath({
                one: {
                    two: 2
                }
            }, ['one', 'two']);
            value.should.be.equal(2);
        });
    });
});