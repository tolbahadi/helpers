'use strict';

require('should');

var parseString = require('../xml2js_wrapper').parseString;

describe('XML2JS_Wrapper', function () {
    it('#exits', function () {
        parseString.should.be.ok;
        parseString.should.be.a.Function;
    });

    it('#throw a custom error', function (done) {
        parseString('lkj').done(function () { done('should throw'); }, function () { done(); });
    });
});