'use strict';

var should = require('should'),
    request = require('../request_wrapper').request;

should = null;

describe('Request Wrapper module', function () {
    it('Fetching google.com', function (done) {
        this.timeout(1000);
        var url = 'http://google.com/';

        request(url).then(function (result) {
            result.contents.should.be.ok;
            result.contents.should.be.String;
            result.duration.should.be.ok;
            result.duration.should.be.Number;
            done();
        }).fail(function (error) {
            done(error instanceof Error ? error : new Error(JSON.stringify(error)));
        });
    });

    it('Testing error wrapper', function (done) {
        request({url: 'google', timeout: 5}).done(function () { done('This should fail'); }, function () { done(); });
    });
});