'use strict';

var request = require('request'),
    Q = require('q'),
    verror = require('verror');

// function that takes a url return a promise that will be resolved with the page content
module.exports.request = function (options) {
    var deferred = Q.defer();
    var startTime = +new Date();

    request(options, function (error, response, body) {
        var duration = +new Date() - startTime;
        if (!error && response.statusCode === 200) {
            deferred.resolve({
                contents: body,
                duration: duration,
                response: response
            });
        } else {
            var errorObject = new verror.WError(error, 'requestWrapperError');

            errorObject.options = options;
            errorObject.response = response;

            if (response && response.statusCode) {
                errorObject.statusCode = response.statusCode;
            }

            deferred.reject(errorObject);
        }
    });
    return deferred.promise;
};