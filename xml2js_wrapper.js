'use strict';

var Q = require('q'),
    parseString = require('xml2js').parseString,
    verror = require('verror');

// function that takes xml feed content and returns json
var parseStringWrapper = function (feedPage) {
    var deferred = Q.defer();

    parseString(feedPage, function (err, result) {
        if (!err) {
            deferred.resolve(result);
        } else {
            var werror = new verror.WError(err, 'xml2jsWrapper:XML parsing error');
            werror.contents = feedPage;
            deferred.reject(werror);
        }
    });

    return deferred.promise;
};

module.exports.parseString = parseStringWrapper;